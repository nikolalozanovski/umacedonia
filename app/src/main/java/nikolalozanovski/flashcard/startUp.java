package nikolalozanovski.flashcard;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IntegerRes;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.util.Vector;
import java.util.concurrent.Semaphore;

/**
 * Created by nikola on 22.5.17.
 */

public final class startUp extends Application {


    @Override
    public void onCreate() {
//        Log.d("create","creating resources");
//        FirebaseApp.initializeApp(getApplicationContext());
//        mAuth = FirebaseAuth.getInstance();
//        mAuth.signInWithEmailAndPassword("ninimkd@gmail.com", "umacedonia123")
//                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        Log.d("auth", "signInWithEmail:onComplete:" + task.isSuccessful());
//
//                        // If sign in fails, display a message to the user. If sign in succeeds
//                        // the auth state listener will be notified and logic to handle the
//                        // signed in user can be handled in the listener.
//                        if (!task.isSuccessful()) {
//                            Log.w("email", "signInWithEmail", task.getException());
//
//                        }
//
//                        // ...
//                    }
//                });
//        final DatabaseReference database = FirebaseDatabase.getInstance().getReference();
//        database.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                for(DataSnapshot o : dataSnapshot.child("Categories").getChildren() ){
//                    Categories.addCategory(o.getKey(),"ancientmacedonia", "ancient_macedonians_background");
//                    Log.d("keys",o.getKey());
//                    //key is the category name
//                    //values the id of the category
//                }
//                if(currentVersion < Integer.parseInt(dataSnapshot.child("Version").child("Latest").getValue(String.class))){
//                    currentVersion = Integer.parseInt(dataSnapshot.child("Version").child("Latest").getValue(String.class));
//
//                    for (DataSnapshot o : dataSnapshot.child("Version").child("Active").getChildren()
//                            ) {
//                        activeVersions.add(Integer.parseInt(o.getValue(String.class)));
//                    }
//
//                    for (int i : activeVersions){
//                        for(DataSnapshot o : dataSnapshot.child(String.valueOf(i)).getChildren())
//                        {
//                            for (DataSnapshot cards : o.getChildren()){
//                                if(cards.getKey().equals("title")){
//                                    //get the values and
//                                    //use title of the card
//                                }
//                                if(cards.getKey().equals("description")){
//                                    for(DataSnapshot description : cards.getChildren()){
//                                        //get all values
//                                        //and use the description
//                                    }
//                                }
//                            }
//                        }
//
//                    }
//
//                    Log.d("cur version", String.valueOf(currentVersion));
//                    Log.d("active versions",String.valueOf(activeVersions.get(0)));
//                    Categories.setUp();
//
//                }
////                semaphore.release();
//                startup=true;
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
////                thread.interrupt();
//            }
//        });
//     thread.run();
try{
//semaphore.acquire();
}catch(Exception e){

}


        Log.d("thread","exiting oncreate");
    }




//    public  startUp(){
//        Log.d("start: ","settings up resources");




//        Vector<Card> myList;

//        Categories.addCategory("Ancient Macedonia", "ancientmacedonia", "ancient_macedonians_background");
//        myList = new Vector<>(Arrays.asList(new Card("Alexander the great","Some card information about him"), new Card("Philip second","some shit about him")));
//        Categories.addCardSet("Ancient Macedonia",myList);
//
//        Categories.addCategory("Medieval Macedonia", "medievalmacedonia","medieval_macedonia_background");
//        myList = new Vector<>(Arrays.asList(new Card("King Marko","Some card information about him"), new Card("Economy of Medieval macedonia","perfect")));
//        Categories.addCardSet("Medieval Macedonia",myList);
//
//        Categories.addCategory("Ottoman Macedonia", "ottomanmacedonia","ottoman_macedonia_background");
//        myList = new Vector<>(Arrays.asList(new Card("Sultan Suleiman","Some card information about him"), new Card("Selim Suleiman","some shit about him")));
//        Categories.addCardSet("Ottoman Macedonia",myList);
//
//        Categories.addCategory( "Yugoslav Macedonia", "yugoslavmacedonia","yugoslav_macedonia_background");
//        myList = new Vector<>(Arrays.asList(new Card("Josip Broz Tito","Some card information about him"), new Card("The mournin of JBT","some shit about him")));
//        Categories.addCardSet("Yugoslav Macedonia",myList);
//
//        Categories.addCategory("Modern Macedonians", "modernmacedonians","modern_macedonians_background");
//        myList = new Vector<>(Arrays.asList(new Card("Zoran Zaev","Some card information about him"), new Card("Nikola Gruevski","some shit about him")));
//        Categories.addCardSet("Modern Macedonians",myList);
//
//        Categories.addCategory("Slavic Migration Myth", "slavicmigrationmyth","slavic_myth");
//        myList = new Vector<>(Arrays.asList(new Card("Over the mountains","Some card information about him"), new Card("To the south","some shit about him")));
//        Categories.addCardSet("Slavic Migration Myth",myList);
//
//        Categories.addCategory("Macedonian Language", "macedonianlanguage","macedonian_languge_background");
//        myList = new Vector<>(Arrays.asList(new Card("Alphabets and characters","Some card information about this"), new Card("syntax","some shit about syntax")));
//        Categories.addCardSet("Macedonian Language",myList);
//
//        Categories.addCategory( "Human Rights", "humanrightsissues","human_rights_background");
//        myList = new Vector<>(Arrays.asList(new Card("Women","Some card information about them"), new Card("Boys","some shit about them")));
//        Categories.addCardSet("Human Rights",myList);
//
//        Categories.addCategory( "Greek Rebuttals", "greekquestions","flag_of_greece");
//        myList = new Vector<>(Arrays.asList(new Card("Name name name","Some card information about name"), new Card("The borders are open","some shit about borders")));
//        Categories.addCardSet("Greek Rebuttals",myList);
//
//        Categories.addCategory( "Bulgarian Rebuttals", "bulgarianquestions","flag_of_bulgaria");
//        myList= new Vector<>(Arrays.asList(new Card("President","Some card information about him"), new Card("Prime minister","some shit about him")));
//        Categories.addCardSet("Bulgarian Rebuttals",myList);
//
//        Categories.addCategory( "Albanian Rebuttals", "albanianquestions","albanians_background");
//        myList = new Vector<>(Arrays.asList(new Card("Ali Ahmeti","Some card information about him"), new Card("Talat Xaferi","some shit about him")));
//        Categories.addCardSet("Albanian Rebuttals",myList);
//
//        Categories.addCategory( "ALL", "all", null);
//        myList= new Vector<>(Arrays.asList(new Card("President","Some card information about him"), new Card("Prime minister","some shit about him")));
//        Categories.addCardSet("ALL",myList);
//    }
}
