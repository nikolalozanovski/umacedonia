package nikolalozanovski.flashcard;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

public class MainActivity extends AppCompatActivity {
    public static int minHeight=1;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference mStorage = storage.getReferenceFromUrl("gs://umacedonia-f33d5.appspot.com/");
    public static Folder fold=new Folder("/sdcard/Flashcard");
    public static Boolean startup=false;
    private FirebaseAuth mAuth;
    private int currentVersion=0;
    private Vector<Integer> activeVersions = new Vector<>();
    String title ="";
    static Vector<String> descriptionVec = new Vector<>();
    static Vector<Card> card = new Vector<>();
    CategoriesAdapter CA=new CategoriesAdapter(this,true);
    CategoriesAdapter CA2=new CategoriesAdapter(this,false);
    RequestQueue queue;
    public static int messageID=-1;
    LinearLayout layout;
    Vector<String> images = new Vector<>();
    FirebaseUser user;
    ListView li;
    ListView li2;



    public void sendNotifications() {
        try {
            queue = Volley.newRequestQueue(MainActivity.this);
            JsonObject notificationData = new JsonObject();
            notificationData.addProperty("body", "new data found");
            notificationData.addProperty("title", "UMacedonia");
            notificationData.addProperty("sound", "off");
            notificationData.addProperty("priority", "high");
            JsonObject params = new JsonObject();
            params.add ("notification", notificationData);
            params.addProperty("to", "/topics/Version");
            JsonObjectRequest req = new JsonObjectRequest("https://fcm.googleapis.com/fcm/send", new JSONObject(params.toString()),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                messageID = response.getInt("message_id");
                                Log.d("Response", response.toString(4));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Error: ", error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap();
                    params.put("Content-Type", "application/json");
                    params.put("authorization", "key=" + "AAAA20O3VSk:APA91bGBtlmmYovX3kwKn4-bdiREpHSQkTCAjTu5WG5psKL0wIHIWFmgsXvSjCt720xlAkDNPvpkgTCsvFHobqZ9iAY4hkXYuCeXh3QvosI7zopwMwrZ-TjLzA2e9QjmblTM6Tb_lWyu");
                    return params;
                }
            };
            queue.add(req);
        } catch (Exception e) {
            Log.d("Notification Error", e.getMessage());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("activity","started");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseMessaging.getInstance().subscribeToTopic("Version");

        li=(ListView)findViewById(R.id.categoriesLV1);
        li2=(ListView)findViewById(R.id.categoriesLV2);

        li.setAdapter(CA);
        li2.setAdapter(CA2);
        if(!startup) {

            startup=true;

            layout = (LinearLayout) this.findViewById(R.id.bck);
            layout.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.apploadingscreenbig));

            FirebaseApp.initializeApp(getApplicationContext());

            mAuth = FirebaseAuth.getInstance();
            mAuth.signInWithEmailAndPassword("ninimkd@gmail.com", "umacedonia123")
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
//                            Log.d("auth", "signInWithEmail:onComplete:" + task.isSuccessful());

                            if (!task.isSuccessful()) {
//                                Log.w("email", "signInWithEmail", task.getException());
                            user=mAuth.getCurrentUser();
                            }
                        }
                    });
            final DatabaseReference database = FirebaseDatabase.getInstance().getReference();
            database.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot o : dataSnapshot.child("Categories").getChildren()) {

                        FileSource fsi = new FileSource(fold,o.child("image").getValue(String.class));
                        FileSource fsb = new FileSource(fold,o.child("background").getValue(String.class));

                         final StorageReference icon = mStorage.child(o.child("image").getValue(String.class));
                         final StorageReference background = mStorage.child(o.child("background").getValue(String.class));

                        icon.getFile(fsi.getFullFile());
                        background.getFile(fsb.getFullFile()).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                // Successfully downloaded data to local file
                                // ...
                                CA.notifyDataSetChanged();
                                CA2.notifyDataSetChanged();

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Handle failed download
                                // ...
                            }
                        });
                        Categories.addCategory(o.child("id").getValue(Integer.class), o.getKey(), o.child("image").getValue(String.class), o.child("background").getValue(String.class));

                    }
                    if (currentVersion < Integer.parseInt(dataSnapshot.child("Version").child("Latest").getValue(String.class))) {
                        currentVersion = Integer.parseInt(dataSnapshot.child("Version").child("Latest").getValue(String.class));
                        sendNotifications();
                        for (DataSnapshot o : dataSnapshot.child("Version").child("Active").getChildren()){
                            activeVersions.add(Integer.parseInt(o.getValue(String.class)));
                        }

                        for (int i : activeVersions) {
                            for (DataSnapshot o : dataSnapshot.child(String.valueOf(i)).getChildren()) {
                                if(!o.getKey().equals("All")){
                                for(DataSnapshot cards : o.getChildren()){
                                    title=cards.child("title").getValue(String.class);
//                                    Log.d("card title is:", title.toString());
                                    for(DataSnapshot desc : cards.child("description").getChildren()){
                                        descriptionVec.add(desc.getValue(String.class));
                                    }
//                                    card.add(new Card(title,descriptionVec));
//                                    Log.d("before:", String.valueOf(card.get(0).getDescription().isEmpty()));
                                    Categories.addCardsToPack(o.getKey(), new Card(title,descriptionVec));
                                    descriptionVec.clear();
                                }
                                card.clear();

                            }

                        }
                        }
                    }

                    Categories.setUp();
                    layout.setBackground( ContextCompat.getDrawable(MainActivity.this, android.R.color.background_light));

                    CA.notifyDataSetChanged();
                    CA2.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    databaseError.getMessage();
                    Log.d("cancelled", "firebase failed to retrive data");
                }

            });
        }

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.maintheme);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        minHeight = displaymetrics.densityDpi/(Categories.getCount()-1);




        li.setDivider(null);
        li.setDividerHeight(38);
        li.setVerticalScrollBarEnabled(false);


        li2.setDivider(null);
        li2.setDividerHeight(38);
        li2.setVerticalScrollBarEnabled(false);




        li.setAdapter(CA);
        li2.setAdapter(CA2);

    }
}

/**
 * instance of one card
 * @title is used to specify the title name of the card
 * @description is used to specify the description of the card
 */
class Card{

    private String title;
    private Vector<String> facts = new Vector<>();

    //construct
    public Card(String Title, Vector<String> Facts) {
        title=Title;
//        facts=Facts;
        facts.addAll(Facts);
    }
    //getter for name
    public String getTitle() {
        return title;
    }
    //getter for ID
    public Vector<String> getDescription() {
        return facts;
    }
}

/**
 * This class is used to instantiate a pack containing multiple cards
 * all cards are placed in linked list
 */

class Pack {
    private  Vector<Card> CardsList=new Vector<>();

    //return how many cards are present in this pack
    public int getCount()
    {
        return CardsList.size();
    }
    //returns the specific item from the linked list
    public  Card getCard(int position){
        if(position==CardsList.size()||position<0){
            return null;
        }
        return CardsList.get(position);
    }
    //method used to add category in the list
    public  void addCard(String title, Vector<String> description)
    {
        CardsList.add(new Card(title,description));
    }
    public void copyList(Vector<Card> cardlist)
    {
        CardsList.addAll(cardlist);
    }

}
/**
 * this class is used to represent one element of categories
 * @ID is used to represent unique identifier for the category, should be unused for now
 * @packOfCards represent one pack of cards
 * @name is used to add text to screen/listview component
 */
class Categories {
    //id of the category
    private int ID;

    //instance of Pack where multiple cards are inserted
    private Pack packOfCards;

    //this field represent text used to display on screen
    private String name;
    //image specified as icon
    private String picture;
    //background image of the card
    private String background;


    //linked list where all categories objects are stored
    private static Vector<Categories> CatList=new Vector<>();

    private static Vector<Categories> CatList1=new Vector<>();
    private static Vector<Categories> CatList2=new Vector<>();


    public String getBackground(){
        return background;
    }

    public Pack getPack(){
        return packOfCards;
    }

    public Pack getPack(int position){
        if(position>CatList.size())
        {
            position=CatList.size();
            return CatList.get(position).getPack();
        }
        else if (position<0){
            position=0;
            return CatList.get(position).getPack();
        }
        else {
            return CatList.get(position).getPack();
        }
    }


    public  String getPicture(){
        return picture;
    }

    public static void setUp(){
        Collections.sort(CatList, new Comparator<Categories>() {
            @Override
            public int compare(Categories o1, Categories o2) {
                return o1.ID-o2.ID;
            }
        });
        int count=0;
        for(Iterator<Categories> it=CatList.iterator();it.hasNext();)
        {

            it.next();
            if((count%2)==0){
                CatList1.add(CatList.get(count));
            }

            if((count%2)!=0){
                CatList2.add(CatList.get(count));
            }
            count++;
        }
//            Collections.reverse(CatList2);
//            Collections.reverse(CatList1);

            Log.d("collections","Collections are reversed");
    }
    //return how many categories are present
    public static int getCount(Boolean align)
    {

        if(align.equals(true)){
            return CatList1.size();
        }
        else if(align.equals(false)){
            return CatList2.size();
        }
        return CatList.size();
    }
    public static int getCount(){
        return CatList.size();
    }
    //returns the specific item from the linked list
    public static Categories getItem(int position, Boolean align){
        if(align){
            return CatList1.get(position);
        }
        else{
            return CatList2.get(position);
        }
    }
    public static Categories getItem(int position){
        if(position<0){
            return CatList.get(0);
        }else if(position>CatList.size()){
            return CatList.get(CatList.size());
        }else {
            return CatList.get(position);
        }
    }
    @Nullable
    public static Categories getSpecificCategory(String Name){
        for(Categories o : CatList){
        if(o.name.equals(Name))
        {
            return o;
        }
        }
        return null;
    }

    //method used to add category in the list
    public static void addCategory(int ID,String name, String picture, String background)
    {
        CatList.add(new Categories(ID, name, picture, background));

    }
    public static void addCardsToPack(String category ,Card card)
    {
        Categories.getSpecificCategory(category).packOfCards.addCard(card.getTitle(),card.getDescription());
    }

    public static void addCardSet(String category, Vector<Card> pack){
        Categories.getSpecificCategory(category).packOfCards.copyList(pack);
    }


    //construct
    private Categories(int id, String Name, String picturePath, String backgroundPath) {
        ID=id;
        name=Name;
        packOfCards = new Pack();
        picture=picturePath;
        background=backgroundPath;
    }
    private Categories() {}
    //getter for name
    public String getName() {
        return name;
    }
    //getter for ID
    public int getID() {
        return ID;
    }

}
//adapter class used for listview
class CategoriesAdapter extends BaseAdapter{
    private Boolean align;
    private Context context;
    //construct
    public CategoriesAdapter(Context context, Boolean Align) {
        this.align=Align;
        this.context=context;
    }
    //methods automatically implemented

    @Override
    public int getCount() {
        return Categories.getCount(this.align);
    }

    @Override
    public Categories getItem(int position) {
         return Categories.getItem(position,this.align);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getID();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
             int drawableID;
            LayoutInflater inflate=LayoutInflater.from(this.context);
            convertView=inflate.inflate(R.layout.categories,parent,false);
            convertView.setMinimumHeight(MainActivity.minHeight);

            String catName=this.getItem(position).getName();
            String pic=this.getItem(position).getPicture();

            TextView thisCategory = (TextView) convertView.findViewById(R.id.txt1);
            ImageView icon=(ImageView) convertView.findViewById(R.id.img1);
            FileInputStream in;
            BufferedInputStream buf;
                thisCategory.setText(catName);
                if(!pic.equals("")) {
                    try {
                        in = new FileInputStream(MainActivity.fold.Location+"/"+getItem(position).getPicture());
                        buf = new BufferedInputStream(in);
                        Bitmap bMap = BitmapFactory.decodeStream(buf);

                        if (in != null) {
                            in.close();
                        }
                        if (buf != null) {
                            buf.close();
                        }
                        icon.setImageBitmap(bMap);
//                        in.reset();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
//                    BitmapFactory.Options options = new BitmapFactory.Options();
//                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//                        FileSource fs = new FileSource(MainActivity.fold, getItem(position).getPicture());
//                        Bitmap bmp = BitmapFactory.decodeFile(fs.getFullFile().getAbsolutePath(), options);
//                        icon.setImageBitmap(bmp);
//                    if(bmp!=null){
//                        Log.d("bmp","not null");
//                    }else{
//                     Log.d("bmp","null");
//                    }
//                    drawableID = context.getResources().getIdentifier(pic, "drawable", BuildConfig.APPLICATION_ID);
//                    icon.setImageResource(drawableID);
                }
//                thisCategory.setText(this.getItem(position).getName());
                thisCategory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent myIntent = new Intent(v.getContext(), One_card.class);
                        myIntent.putExtra("category", Categories.getItem(position, align).getName());
                        v.getContext().startActivity(myIntent);
                    }
                });
            icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent myIntent = new Intent(v.getContext(), One_card.class);
                    myIntent.putExtra("category", Categories.getItem(position, align).getName());
                    v.getContext().startActivity(myIntent);
                }
            });
//        }
        return convertView;
    }
}