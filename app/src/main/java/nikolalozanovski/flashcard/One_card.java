package nikolalozanovski.flashcard;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.StackView;
import android.widget.TextView;

import java.util.Vector;

/**
 * Created by nikola on 2.5.17.
 */

public class One_card extends AppCompatActivity {
    private static int CardPosition;
    private static int PackPosition;
    private static Pack p;
    private Categories category;
    private boolean cardSwitch=false;
    private ImageView background;
    private boolean isFront=true;

    private  View backSide;
    private  View frontSide;
    static String Category;
    RelativeLayout layout;
    RelativeLayout backlayout;
    static Card c;

    TextView title;

    Button ges;
    Button ges2;

    private void setevents(){
        Log.i("isfront",String.valueOf(isFront));
        ges = (Button) findViewById(R.id.gesture);

        ges.setOnTouchListener(new OnSwipeTouchListener(One_card.this){

            @Override
            public void DoubleTap(){

//                <Button
//                android:layout_width="match_parent"
//                android:layout_height="match_parent"
//                android:id="@+id/gesture"
//                android:visibility="visible"
//                android:alpha="0"
//                        />

                isFront=false;
                setContentView(backSide);
                ListView descipt = (ListView) findViewById(R.id.facts);
                descipt.setDividerHeight(8);
                descipt.setVerticalScrollBarEnabled(true);
                descipt.setOnTouchListener((new OnSwipeTouchListener(One_card.this){
                    public void DoubleTap(){
                        setContentView(R.layout.one_card);
                        title=(TextView) findViewById(R.id.CardTitle);
                        title.setText(p.getCard(CardPosition).getTitle());
                        setBackgroundImage();
                        setevents();
                        isFront=true;
                    }
                }));
                Log.d("CardPosition",String.valueOf(CardPosition));
                Log.d("PackPosition",String.valueOf(PackPosition));
                descriptionAdapter des = new descriptionAdapter(c.getDescription(),One_card.this);
                descipt.setAdapter(des);
//                setBackgroundImage();


            }
            @Override
            public void onSwipeLeft() {
                Log.d("swipe","kon levo");
                cardSwitch=false;
                CardPosition++;
                if(Category.equals("ALL")) {
                    if (CardPosition >= p.getCount()) {
                        CardPosition = 0;
                        PackPosition++;
                        Log.d("Pack position",String.valueOf(PackPosition));
                        Log.d("Pack size",String.valueOf(Categories.getCount()));

                        if (PackPosition > Categories.getCount()-2||(PackPosition > Categories.getCount()-2&&CardPosition>p.getCount())) {
                            PackPosition = Categories.getCount()-2;
                            CardPosition = p.getCount();
                        }

                        category = Categories.getItem(PackPosition);
                        Log.d("background:", category.getBackground());
//                        if(PackPosition<11) {
                        setBackgroundImage();
//                        }
                        p = category.getPack();
                    }
                }else{
                    if(CardPosition>=p.getCount()){
                        CardPosition=p.getCount()-1;
                    }
                }

                c = p.getCard(CardPosition);
                if(c!=null) {
                    title.setText(c.getTitle());
//                    desc.setText(c.getDescription());
                }else{
                    if(PackPosition==Categories.getCount()-2&&CardPosition>p.getCount()-1){
                        CardPosition=p.getCount()-1;
                    }
                }
                Log.d("CardPosition",String.valueOf(CardPosition));
                Log.d("PackPosition",String.valueOf(PackPosition));
            }
            @Override
            public void onSwipeRight() {
                Log.d("swipe","kon desno");
                cardSwitch=false;
                CardPosition--;

                if(Category.equals("ALL")) {
                    if (CardPosition < 0) {
                        PackPosition--;
                        cardSwitch=true;
                        if (PackPosition < 0) {
                            PackPosition = 0;
                            cardSwitch=false;
                        }
                        category = Categories.getItem(PackPosition);
                        Log.d("background:", category.getBackground());
                        setBackgroundImage();

                        p = category.getPack();

                        if(cardSwitch) {
                            CardPosition = p.getCount()-1;
                        }

                    }
                }
                Card c = p.getCard(CardPosition);
                if(c!=null) {
                    title.setText(c.getTitle());
//                    desc.setText(c.getDescription());
                }else{
                    if(!Category.equals("ALL")) {
                        CardPosition = 0;
                    }else {
                        if(CardPosition<=0&&PackPosition<=0){
                            CardPosition=0;
                            return;
                        }
                        CardPosition = p.getCount();
                    }
                }
                Log.d("CardPosition",String.valueOf(CardPosition));
                Log.d("PackPosition",String.valueOf(PackPosition));
            }
        });
    }

    private void setBackgroundImage(){
        Log.d("background:", category.getBackground());
        background = (ImageView) findViewById(R.id.bckground);
        final FileSource fs = new FileSource(MainActivity.fold,category.getBackground());
//        background.setImageResource(this.getResources().getIdentifier(category.getBackground(), "drawable", BuildConfig.APPLICATION_ID));
        Bitmap bmp = BitmapFactory.decodeFile(fs.getFullFile().getAbsolutePath());
        background.setImageBitmap(bmp);
    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.one_card);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.maintheme);


        layout = new RelativeLayout(this);
        backSide = getLayoutInflater().inflate(R.layout.backside, null);
        frontSide = getLayoutInflater().inflate(R.layout.one_card, null);
        backlayout=(RelativeLayout)backSide.findViewById(R.id.rl);

        layout.setBackgroundColor(Color.BLACK);
        layout.setClickable(true);

        Category=getIntent().getStringExtra("category");
        category = Categories.getSpecificCategory(Category);

        CardPosition=0;
        PackPosition=0;

        title=(TextView) findViewById(R.id.CardTitle);
        background= (ImageView) findViewById(R.id.bckground);
        ges = (Button) findViewById(R.id.gesture);
        ges2 = (Button) backSide.findViewById(R.id.gesture);

        RelativeLayout rel = (RelativeLayout) backSide.findViewById(R.id.rl);
        rel.setOnTouchListener((new OnSwipeTouchListener(One_card.this){
            public void DoubleTap(){
                setContentView(R.layout.one_card);
                title=(TextView) findViewById(R.id.CardTitle);
                title.setText(p.getCard(CardPosition).getTitle());
                setBackgroundImage();
                setevents();
                isFront=true;
            }
        }));

        Log.d("background:", category.getBackground());
        if(!Category.equals("ALL")) {
            p = category.getPack();
        }else{
            p= category.getPack(PackPosition);
        }
        c=p.getCard(CardPosition);




        if(!Category.equals("ALL")){
            FileSource fs = new FileSource(MainActivity.fold,category.getBackground());
            Bitmap bmp = BitmapFactory.decodeFile(fs.getFullFile().getAbsolutePath());
            background.setImageBitmap(bmp);
        }else{
            FileSource fs = new FileSource(MainActivity.fold,Categories.getItem(0).getBackground());
            Bitmap bmp = BitmapFactory.decodeFile(fs.getFullFile().getAbsolutePath());
//            background.setImageResource(this.getResources().getIdentifier(Categories.getItem(0).getBackground(), "drawable", BuildConfig.APPLICATION_ID));
            background.setImageBitmap(bmp);
        }

//        title.setText(Categories.getSpecificCategory(Category).getPack().getCard(CardPosition).getTitle());
        title.setText(p.getCard(CardPosition).getTitle());


        Log.d("Card position",String.valueOf(CardPosition));
        Log.d("Pack position",String.valueOf(PackPosition));
        setevents();
    }
}
class descriptionAdapter extends BaseAdapter{
    private Vector<String> description = new Vector();
    private Context context;
    public descriptionAdapter(Vector<String> desc, Context c){
        description=desc;
        context=c;
    }

    @Override
    public int getCount() {
        return description.size();
    }

    @Override
    public Object getItem(int position) {
        return description.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflate=LayoutInflater.from(this.context);
        convertView=inflate.inflate(R.layout.backsideelement,parent,false);
        TextView descript = (TextView) convertView.findViewById(R.id.descElem);
        descript.setTypeface(Typeface.createFromAsset(context.getAssets(), "arial.ttf"));
        descript.setText("\u2022 "+this.getItem(position).toString());
        return convertView;
    }
}
class OnSwipeTouchListener implements OnTouchListener {

    private final GestureDetector gestureDetector;

    public OnSwipeTouchListener (Context ctx){
        gestureDetector = new GestureDetector(ctx, new GestureListener());
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }


    private final class GestureListener extends SimpleOnGestureListener {

        private static final int SWIPE_THRESHOLD = 50;
        private static final int SWIPE_VELOCITY_THRESHOLD = 50;

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            boolean result = false;
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight();
                        } else {
                            onSwipeLeft();
                        }
                        result = true;
                    }
                }
                else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        onSwipeBottom();
                    } else {
                        onSwipeTop();
                    }
                    result = true;
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return result;
        }
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            float x = e.getX();
            float y = e.getY();

            Log.d("Double Tap", "Tapped at: (" + x + "," + y + ")");
            DoubleTap();
            return true;
        }
    }

    public void onSwipeRight() {

    }

    public void onSwipeLeft() {

    }

    public void onSwipeTop() {
    }

    public void onSwipeBottom() {
    }
    public void DoubleTap(){

    }

}